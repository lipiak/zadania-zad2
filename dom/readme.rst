
Praca domowa #2
===============

Stworzyć w pełni funkcjonalny serwer HTTP.

* Rozwinięcie URI względem katalogu ``web``
  (np. URI w postaci ``/dir/file.txt`` oznacza plik ``file.txt`` w podkatalogu ``dir`` w katalogu ``web``).

  * Jeżeli URI wskazuje na katalog, to serwer zwraca listę plików i podkatalogów w danym katalogu wraz z kodem 200
    w formatowaniu HTML z klikalnymi linkami do plików i katalogów.
  * Jeżeli URI wskazuje na plik, to serwer zwraca zawartość tego pliku z kodem 200
    oraz odpowiednim nagłówkiem ``Content-Type``
    (obsługiwane pliki: .html, .txt, .jpg, .png; zobacz moduł ``mimetypes``).
  * Jeżeli URI nie istnieje to serwer zwraca odpowiedź z kodem 404.

* W przypadku wystąpienia błędu w programie, serwer powinien zwracać status ``500 Internal Server Error``.

* Stworzyć kompleksowe testy sprawdzające poprawność działania serwera.