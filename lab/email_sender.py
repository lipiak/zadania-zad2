# -*- coding: utf-8 -*-
import smtplib
import email.utils

from email.mime.text import MIMEText

def create_and_send_email():
    """Funkcja interaktywna pytająca użytkownika o potrzebne dane
    i wysyłająca na ich podstawie list elektroniczny.
    """
    # Odczyt danych od użytkownika
    from_addr = raw_input ('Podaj swoj email: ')
    to_addrs = raw_input('Podaj email odbiorcy: ')
    subject = raw_input('Tytuł wiadomości: ')
    message= raw_input('Treść: ')

    # Stworzenie wiadomości
    msg = MIMEText(message, _charset='utf-8')
    msg['From'] = email.utils.formataddr(("Jan Kowalski", from_addr))
    msg['To'] = email.utils.formataddr(("Marian Marian", to_addrs))
    msg['Subject'] = subject

    try:
        # Połączenie z serwerem pocztowym
        server = smtplib.SMTP('194.29.175.240', 25)

        # Ustawienie parametrów
        server.set_debuglevel(True)
        server.ehlo()

        # Autentykacja
        server.starttls()
        server.login('p5', 'p5')

        # Wysłanie wiadomości
        server.sendmail(from_addr, [to_addrs, ], msg.as_string())

        pass

    finally:
        # Zamknięcie połączenia
        server.close()
        pass


if __name__ == '__main__':
    decision = 't'
    while decision in ['t', 'T', 'y', 'Y']:
        create_and_send_email()
        decision = raw_input(u'Wysłać jeszcze jeden list? ')
